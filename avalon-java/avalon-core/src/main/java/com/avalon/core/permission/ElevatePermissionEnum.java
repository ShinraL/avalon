package com.avalon.core.permission;

/**
 * @author lwlianghehe@gmail.com
 * @date 2024/11/29 8:42
 */
public enum ElevatePermissionEnum {
    permission, // 模型权限
    recordRule // 记录权限
}
