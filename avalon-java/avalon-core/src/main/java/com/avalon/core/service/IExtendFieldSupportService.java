package com.avalon.core.service;

import com.avalon.core.field.FieldList;

/**
 * @author lwlianghehe@gmail.com
 * @date 2024/12/01 19:43
 */
public interface IExtendFieldSupportService {
    FieldList getExtendField(String service);
}
