/**
 * @author lwlianghehe@gmail.com
 * @date 2024/11/22
 */

package com.avalon.core.service;

import java.util.List;

public interface IModuleSupport {
    List<String> getInstalledModule();
}
