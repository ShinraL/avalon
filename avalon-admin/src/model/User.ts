/**
 * @author lwlianghehe@gmail.com
 * @date 2024/11/22
 */

export default interface User {
    id: Number
    name: string
    nickName: string
    phone: string
    sex: string
    avatar: string,
    debug: boolean,
    birthday: string,
    account: string
}
