/**
 * @author lwlianghehe@gmail.com
 * @date 2025/01/15 14:26
 */
import Field from "./Field.ts";

export default interface ShowField {
    Field: Field,
    originField: string,
    serviceName: string
}