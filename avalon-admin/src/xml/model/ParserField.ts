/**
 * @author lwlianghehe@gmail.com
 * @date 2024/12/16 20:00
 */

export interface ParserField {
    name: string;
    widget: string
}