export default interface ChatMember {
    id: number
    imUserId: number
    name: string
    avatar: string,
}
